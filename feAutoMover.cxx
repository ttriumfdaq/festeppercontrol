/********************************************************************\
Crude program to make the emulsion moving table move after each spill.
Jan 2018
\********************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include "midas.h"
#include <stdint.h>
#include <iostream>
#include <unistd.h>

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "feAutoMover";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size =  3 * 1024 * 1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) --- not really used here */
INT max_event_size_frag = 2 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 20 * 1000000;
 void **info;
char  strin[256];
HNDLE hDB, hSet;


/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_auto_event(char *pevent, INT off);

/*-- Equipment list ------------------------------------------------*/

#undef USE_INT

EQUIPMENT equipment[] = {

   {"AutoMover",               /* equipment name */
    {1, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC,              /* equipment type */
     LAM_SOURCE(0, 0xFFFFFF),        /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING,           /* read only when running */
     100,                    /* poll for 1000ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_auto_event,      /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

HNDLE gKeySpillNumber; // pointer to ODB Spill Number
int gSpillNumber; // spill number

HNDLE gKeyEnable; // pointer to ODB Auto Mover enable
BOOL gEnable; // auto mover enable

HNDLE gKeyStepSize; // pointer to ODB mover step size
double gStepSize; // step size (in mm)

BOOL spillFinished = FALSE;

HNDLE gKeyTablePosition; // pointer to ODB table position
HNDLE gKeyTableDestination; // pointer to ODB table destination

// New spill callback
void new_spill(INT hDB, INT hKey, void *info){  
  printf("Spill finished. spill # = %i\n",gSpillNumber);
  if(gSpillNumber >=0){ // spillNumber gets reset to -1 at start of run.
    spillFinished = TRUE;
  }
}

// Enable callback
void enable_callback(INT hDB, INT hKey, void *info){  
  if(gEnable){
    cm_msg(MINFO,"callback",Auto move enabled\n");
  } else{
    cm_msg(MINFO,"callback",Auto move disabled\n");
  }
}

// Step size  callback
void stepsize_callback(INT hDB, INT hKey, void *info){  
  cm_msg(MINFO,"stepsize_callback","Auto-mover changed step size to %f mm\n",gStepSize);
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{

  int size = sizeof(int);  

  // Setup hotlinks on the spill number from beamStatus, 
  // the auto-moving enable and the step size (in mm)

  // spill number
  int status = db_find_key(hDB, 0, "/Equipment/BeamStatus/NewSpill/spillNumber", &gKeySpillNumber);
  if(status != DB_SUCCESS){ printf("Something went wrong getting key\n"); return 0;}
  db_get_data(hDB, gKeySpillNumber, &gSpillNumber, &size, TID_INT);
  db_open_record(hDB,gKeySpillNumber,&gSpillNumber,sizeof(int),MODE_READ,
                 new_spill,NULL);

  // Enable
  status = db_find_key(hDB, 0, "/Equipment/AutoMover/Settings/Enable", &gKeyEnable);
  if(status != DB_SUCCESS){ printf("Something went wrong getting key\n"); return 0;}
  size = sizeof(BOOL);
  db_get_data(hDB, gKeyEnable, &gEnable, &size, TID_BOOL);
  db_open_record(hDB,gKeyEnable,&gEnable,sizeof(BOOL),MODE_READ,
                 enable_callback,NULL);

  // StepSize
  status = db_find_key(hDB, 0, "/Equipment/AutoMover/Settings/StepSize", &gKeyStepSize);
  if(status != DB_SUCCESS){ printf("Something went wrong getting key\n"); return 0;}
  size = sizeof(double);
  db_get_data(hDB, gKeyStepSize, &gStepSize, &size, TID_DOUBLE);
  db_open_record(hDB,gKeyStepSize,&gStepSize,sizeof(double),MODE_READ,
                 stepsize_callback,NULL);

  // Get key handles to the table position and destination
  // /Equipment/MovingTable/Variables/STEP[0]
  status = db_find_key(hDB, 0, "/Equipment/MovingTable/Variables/STEP", &gKeyTablePosition);
  if(status != DB_SUCCESS){ printf("Something went wrong getting key position\n"); return 0;}
  status = db_find_key(hDB, 0, "/Equipment/MovingTable/Settings/Destination", &gKeyTableDestination);
  if(status != DB_SUCCESS){ printf("Something went wrong getting key destination\n"); return 0;}



  spillFinished = FALSE;
  return SUCCESS;
}




/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
  spillFinished = FALSE;
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/
// Not currently used for DCRC readout
extern "C" { INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;

   for (i = 0; i < count; i++) {
//      cam_lam_read(LAM_SOURCE_CRATE(source), &lam);

//      if (lam & LAM_SOURCE_STATION(source))
         if (!test)
            return 1;
   }

   usleep(1000);
   return 0;
}
}

/*-- Interrupt configuration ---------------------------------------*/
// This is not currently used by the DCRC readout
extern "C" { INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}
}



/*-- Event readout -------------------------------------------------*/
INT read_auto_event(char *pevent, INT off)
{

  if(spillFinished){
    spillFinished = FALSE;
    if(gEnable){

      int size = sizeof(double);
      double position;
      db_get_data_index(hDB, gKeyTablePosition, &position, &size, 0, TID_DOUBLE);
  
      // Make sure the current position is sensible.
      if(position < -5 || position > 330){
	cm_msg(MERROR,"read_auto_event","Current position (%f mm) is outside allowed range of 0-320mm.  You need to reinitialize.",position);
	return 0;
      }
      double next_position = position + gStepSize;
      if(next_position > 310){
	cm_msg(MERROR,"read_auto_event","Next position (%f mm) is past the moving table limit.  Time to end run!",next_position);
	return 0 ;
      }

      cm_msg(MINFO,"read_auto_event","Making auto-move from position = %f mm to %f mm.",position,next_position);
      size = sizeof(double);
      db_set_data(hDB, gKeyTableDestination, &next_position, sizeof(double), 1, TID_DOUBLE);
      //      db_set_data1(hDB, gKeyZeroPosition, &gZeroPosition, sizeof(BOOL), 1,  TID_BOOL);

      /* init bank structure */
      bk_init32(pevent);
      
      double *pdata;
      /* create a bank with spill number, current position and next position  */
      bk_create(pevent, "AUTO", TID_DOUBLE, (void **)&pdata);
      *pdata++ = gKeySpillNumber;
      *pdata++ = position;
      *pdata++ = next_position;      
      
      size = bk_close(pevent, pdata);          
      return bk_size(pevent);

    }
  }

  return 0;
}



