/********************************************************************\
MIDAS frontend for controlling PhidgetStepper Bipolar HC(1067) 
stepper motor controller.
 Thomas Lindner (TRIUMF)
\********************************************************************/


//#include <vector>
#include <stdio.h>
//#include <algorithm>
#include <stdlib.h>
#include "midas.h"
#include <stdint.h>
//#include <iostream>
//#include <sstream>
#include <unistd.h>


/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

  // Phidget functions
#include <stdio.h>
#include <stdlib.h>
#include <phidgetFunctions.h>

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "feStepperMotor";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size =  3 * 1024 * 1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) --- not really used here */
INT max_event_size_frag = 2 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 20 * 1000000;
 void **info;
char  strin[256];
HNDLE hDB, hSet;





/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);


/*-- Equipment list ------------------------------------------------*/

#undef USE_INT

EQUIPMENT equipment[] = {

   {"StepperControl",               /* equipment name */
    {1, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
#ifdef USE_INT
     EQ_INTERRUPT,           /* equipment type */
#else
     EQ_PERIODIC,              /* equipment type */
#endif
     LAM_SOURCE(0, 0xFFFFFF),        /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_ALWAYS | RO_ODB,           /* read only when running */
     500,                    /* poll for 1000ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_trigger_event,      /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

#define StepperControlSETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"MotorSpeed = DOUBLE : 500",\
"MotorAccel = DOUBLE : 10000",\
"Destination = DOUBLE : 0",\
"StopNow = BOOL : N",\
"ZeroPosition = BOOL : N",\
"",\
NULL }

StepperControlSETTINGS_STR(steppersettings_str);


PhidgetStepperHandle fChanHandle;

HNDLE gKeyMotorSpeed; // pointer to ODB variable for motor speed
double gMotorSpeed; // current motor speed
HNDLE gKeyMotorAccel; // pointer to ODB variable for motor accel
double gMotorAccel; // current motor accel
HNDLE gKeyDestination; // pointer to ODB variable for target position
double gDestination; // current target position
HNDLE gKeyStopNow; // pointer to ODB variable for stop now
BOOL gStopNow; // stop now
HNDLE gKeyZeroPosition; // pointer to ODB variable for zero position
BOOL gZeroPosition; // zero the current position

double gCurrentPosition = 0.0; // current position
int gInMotion = 0;

// Change velocity callback
void set_motor_speed(INT hDB, INT hKey, void *info){  
  PhidgetStepper_setVelocityLimit(fChanHandle, gMotorSpeed);
  cm_msg(MINFO,"set_motor_speed","Setting maximum motor speed to %f", gMotorSpeed);
}

// Change acceleration callback
void set_motor_accel(INT hDB, INT hKey, void *info){  
  PhidgetStepper_setAcceleration(fChanHandle, gMotorAccel);
  cm_msg(MINFO,"set_motor_accel","Setting motor acceleration to %f", gMotorAccel);
}

// Change destination callback
void set_destination(INT hDB, INT hKey, void *info){  
  if(gInMotion){ // make sure we aren't already in motion
    cm_msg(MINFO,"set_destination","Can't start a new move; already in a move");
    return;
  }
  gInMotion = 1;
  PhidgetStepper_setTargetPosition(fChanHandle, gDestination);
  cm_msg(MINFO,"set_destination","Starting move to target position to %i", gDestination);
}


// StopNow callback
void stop_now(INT hDB, INT hKey, void *info){  

  if(gStopNow){
    
    printf("Stop now!!! %i\n",gStopNow);
    // Complicated procedure
    // set velocity to zero
    PhidgetStepper_setVelocityLimit(fChanHandle, 0);
    usleep(200000);
    
    // Read the current position and set the target position to current position
    double position, target;
    PhidgetStepper_getPosition(fChanHandle,&position);
    PhidgetStepper_getTargetPosition(fChanHandle,&target);
    printf("Target position = %f, current position = %f\n",target, position);
    PhidgetStepper_setTargetPosition(fChanHandle, position-1);
    
    // Reset velocity to original value
    PhidgetStepper_setVelocityLimit(fChanHandle, gMotorSpeed);

    // Tell it to move again to this position... this somehow necessary to get the
    // readback state of the motor correct... this seems like a bug in the
    // phidget code, where the state of the motion never is quite right...
    PhidgetStepper_setTargetPosition(fChanHandle, position);

    gStopNow = FALSE;
    db_set_data1(hDB, gKeyStopNow, &gStopNow, sizeof(BOOL), 1,  TID_BOOL);
    cm_msg(MINFO,"stop_now","Stopped Stepper");
  }
}


// ZeroPosition callback
void zero_position(INT hDB, INT hKey, void *info){  

  printf("Zeroing position.\n");
  if(gZeroPosition){
    
    printf("Zero Position!!! %i\n",gZeroPosition);

    // Get the current position
    double position;
    PhidgetStepper_getPosition(fChanHandle,&position);

    // Correction is opposite to this.
    double pos_correction = -position;    
    PhidgetStepper_addPositionOffset(fChanHandle,pos_correction);

    // Set the current position to zero in ODB
    gDestination = 0; 
    db_set_data1(hDB, gKeyDestination, &gDestination, sizeof(double), 1, TID_DOUBLE);

    // reset the flag
    gZeroPosition = FALSE;
    db_set_data1(hDB, gKeyZeroPosition, &gZeroPosition, sizeof(BOOL), 1,  TID_BOOL);
    cm_msg(MINFO,"zero_position","Zeroed the Moving Table position at current point.");
  }
}


// Phidget position callback
static void CCONV
onPositionChangeHandler(PhidgetStepperHandle ch, void *ctx, double position) {
  gCurrentPosition = position;
  printf("Position Changed: %.0lf\n", position);
}

static void CCONV
onVelocityChangeHandler(PhidgetStepperHandle ch, void *ctx, double velocity) {
  printf("Velocity Changed: %.0lf\n", velocity);
}

static void CCONV
onStoppedHandler(PhidgetStepperHandle ch, void *ctx) {
  cm_msg(MINFO,"set_motor_speed","Motor stopped... finished move to position to %f", gCurrentPosition);
  gInMotion = 0;
}


/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{

  int size;  
  int status = db_check_record(hDB, 0, "/Equipment/StepperControl/Settings",
                               strcomb(steppersettings_str), TRUE);
  printf("Status %i\n",status);
  if (status == DB_STRUCT_MISMATCH) {
    cm_msg(MERROR, "init_simdaqsettings", "Aborting on mismatching /Equipment/StepperControl/Settings");
    cm_disconnect_experiment();
    abort();
  }



  // set up hot-links (open records) so that functions will get called
  // also get current values of these settings
  // velocity
  status = db_find_key(hDB, 0, "/Equipment/StepperControl/Settings/MotorSpeed", &gKeyMotorSpeed);
  if(status != DB_SUCCESS){
    printf("Something went wrong getting key\n"); return 0;
  }

  db_get_data(hDB, gKeyMotorSpeed, &gMotorSpeed, &size, TID_DOUBLE);

  db_open_record(hDB,gKeyMotorSpeed,&gMotorSpeed,sizeof(double),MODE_READ,
                 set_motor_speed,NULL);

  // speed
  status = db_find_key(hDB, 0, "/Equipment/StepperControl/Settings/MotorAccel", &gKeyMotorAccel);
  db_get_data(hDB, gKeyMotorAccel, &gMotorAccel, &size, TID_DOUBLE);
  db_open_record(hDB,gKeyMotorAccel,&gMotorAccel,sizeof(double),MODE_READ,
                 set_motor_accel,NULL);

  // destination
  status = db_find_key(hDB, 0, "/Equipment/StepperControl/Settings/Destination", &gKeyDestination);
  gDestination = 0; // set the destination to 0 to start, since that is what controller will assume.
  db_set_data(hDB, gKeyDestination, &gDestination, sizeof(double), 1, TID_DOUBLE);
  db_get_data(hDB, gKeyDestination, &gDestination, &size, TID_DOUBLE);
  db_open_record(hDB,gKeyDestination,&gDestination,sizeof(double),MODE_READ,
                 set_destination,NULL);

  // stop now
  status = db_find_key(hDB, 0, "/Equipment/StepperControl/Settings/StopNow", &gKeyStopNow);
  db_open_record(hDB,gKeyStopNow,&gStopNow,sizeof(BOOL),MODE_READ,
                 stop_now,NULL);
  gStopNow = FALSE;
  db_set_data1(hDB, gKeyStopNow, &gStopNow, sizeof(BOOL), 1,  TID_BOOL);

  // zero position
  status = db_find_key(hDB, 0, "/Equipment/StepperControl/Settings/ZeroPosition", &gKeyZeroPosition);
  db_open_record(hDB,gKeyZeroPosition,&gZeroPosition,sizeof(BOOL),MODE_READ,
                 zero_position,NULL);
  gZeroPosition = FALSE;
  db_set_data1(hDB, gKeyZeroPosition, &gZeroPosition, sizeof(BOOL), 1,  TID_BOOL);


  
  // Setup the phidget

  PhidgetReturnCode res;
  const char *errs;
  
  // Enable logging to stdout
  PhidgetLog_enable(PHIDGET_LOG_INFO, NULL);

  res = PhidgetStepper_create(&fChanHandle);
  if (res != EPHIDGET_OK) {
    fprintf(stderr, "failed to create stepper channel\n");
    exit(1);
  }
  
  res = initChannel((PhidgetHandle)fChanHandle, 506380);
  if (res != EPHIDGET_OK) {
    Phidget_getErrorDescription(res, &errs);
    fprintf(stderr, "failed to initialize channel:%s\n", errs);
    exit(1);
  }
  
  res = PhidgetStepper_setOnPositionChangeHandler(fChanHandle, onPositionChangeHandler, NULL);
  if (res != EPHIDGET_OK) {
    Phidget_getErrorDescription(res, &errs);
    fprintf(stderr, "failed to set position change handler: %s\n", errs);
    return 99;
  }
  
  res = PhidgetStepper_setOnVelocityChangeHandler(fChanHandle, onVelocityChangeHandler, NULL);
  if (res != EPHIDGET_OK) {
    Phidget_getErrorDescription(res, &errs);
    fprintf(stderr, "failed to set velocity change handler: %s\n", errs);
    return 98;
  }
  
  res = PhidgetStepper_setOnStoppedHandler(fChanHandle, onStoppedHandler, NULL);
  if (res != EPHIDGET_OK) {
    Phidget_getErrorDescription(res, &errs);
    fprintf(stderr, "failed to set stopped handler: %s\n", errs);
    return 97;
  }

  /*
   * Open the channel synchronously: waiting a maximum of 5 seconds.
   */
  res = Phidget_openWaitForAttachment((PhidgetHandle)fChanHandle, 5000);
  if (res != EPHIDGET_OK) {
    if (res == EPHIDGET_TIMEOUT) {
      printf("Channel did not attach after 5 seconds: please check that the device is attached\n");
    } else {
      Phidget_getErrorDescription(res, &errs);
      fprintf(stderr, "failed to open channel:%s\n", errs);
    }
    return 96;
  }
  
  
  
  
  printf("Engaging the motor\n");
  PhidgetStepper_setEngaged(fChanHandle, 1);

  // Now set the rescale factor... this is very important to get right.
  // Will hardcode it here, since it is only valid for my use case.
  PhidgetStepper_setRescaleFactor(fChanHandle,0.00010127);
  
  printf("Set parameters\n");
  PhidgetStepper_setCurrentLimit(fChanHandle, 1.0);
  
  PhidgetStepper_setVelocityLimit(fChanHandle, gMotorSpeed);
  PhidgetStepper_setAcceleration(fChanHandle, gMotorAccel);
  
  double accel, vel;
  PhidgetStepper_getAcceleration(fChanHandle, &accel);
  PhidgetStepper_getVelocityLimit(fChanHandle, &vel);

  
  printf("Max velocity: %f.  Accel: %f\n",vel, accel);
  cm_msg(MINFO,"frontend_init","Max velocity: %f.  Accel: %f\n",vel, accel);

  //printf("\nSetting Target Position to 15000 for 5 seconds...\n");
  //PhidgetStepper_setTargetPosition(fChanHandle, 5000);

  
  return SUCCESS;
}




/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  printf("Exiting feStepperControl!\n");

  
  Phidget_close((PhidgetHandle)fChanHandle);
  PhidgetStepper_delete(&fChanHandle);
  
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
// Upon run stasrt, read ODB settings and write them to DCRC
INT begin_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
  usleep(1000);
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/
// Not currently used for DCRC readout
#ifdef __cplusplus
extern "C" {
#endif

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;

   for (i = 0; i < count; i++) {
//      cam_lam_read(LAM_SOURCE_CRATE(source), &lam);

//      if (lam & LAM_SOURCE_STATION(source))
         if (!test)
            return 1;
   }

   usleep(1000);
   return 0;
}
#ifdef __cplusplus
}
#endif

/*-- Interrupt configuration ---------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
  INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}
#ifdef __cplusplus
}
#endif


#include <sys/time.h>
/*-- Event readout -------------------------------------------------*/
INT read_trigger_event(char *pevent, INT off)
{


  // Check the current move status
  int isMoving;
  double cvelocity;
  PhidgetStepper_getIsMoving(fChanHandle, &isMoving);
  PhidgetStepper_getVelocity(fChanHandle, &cvelocity);
  //gInMotion = isMoving;
  //  printf("moving=%i, %f\n",isMoving,cvelocity);

  /* init bank structure */
  bk_init32(pevent);
  
  double *pdata32;
  bk_create(pevent, "STEP", TID_DOUBLE, (void **)&pdata32);
 
  *pdata32++ = gCurrentPosition;
  *pdata32++ = gMotorSpeed;
  *pdata32++ = (double)gInMotion;

  int size2 = bk_close(pevent, pdata32);    

  

  struct timeval start,end;
  gettimeofday(&start,NULL);
  if(0)printf ("simdaq request: %f\n",start.tv_sec
      + 0.000001*start.tv_usec); 
  // close the bank

  
  usleep(1000);
  return bk_size(pevent);
}



