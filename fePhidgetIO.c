/********************************************************************\
MIDAS frontend for reading PhidgetStepper IO Board (1012_2B)
 Thomas Lindner (TRIUMF)
\********************************************************************/


//#include <vector>
#include <stdio.h>
//#include <algorithm>
#include <stdlib.h>
#include "midas.h"
#include <stdint.h>
//#include <iostream>
//#include <sstream>
#include <unistd.h>

// IO board serial number: 501372

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

  // Phidget functions
#include <stdio.h>
#include <stdlib.h>
#include <phidgetFunctions.h>

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fePhidgetIO";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size =  3 * 1024 * 1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) --- not really used here */
INT max_event_size_frag = 2 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 20 * 1000000;
 void **info;
char  strin[256];
HNDLE hDB, hSet;





/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);


/*-- Equipment list ------------------------------------------------*/

#undef USE_INT

EQUIPMENT equipment[] = {

   {"PhidgetIO",               /* equipment name */
    {1, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
#ifdef USE_INT
     EQ_INTERRUPT,           /* equipment type */
#else
     EQ_PERIODIC,              /* equipment type */
#endif
     LAM_SOURCE(0, 0xFFFFFF),        /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_ALWAYS | RO_ODB,           /* read only when running */
     1000,                    /* poll for 1000ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_trigger_event,      /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

#define PhidgetIOControlSETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"MotorSpeed = DOUBLE : 500",\
"MotorAccel = DOUBLE : 10000",\
"Destination = INT : 0",\
"",\
NULL }

PhidgetIOControlSETTINGS_STR(iosettings_str);


PhidgetDigitalInputHandle fInputHandle[16];  // 16 separate input handlers...



/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{

  /*  int size;  
  int status = db_check_record(hDB, 0, "/Equipment/StepperControl/Settings",
                               strcomb(steppersettings_str), TRUE);
  printf("Status %i\n",status);
  if (status == DB_STRUCT_MISMATCH) {
    cm_msg(MERROR, "init_simdaqsettings", "Aborting on mismatching /Equipment/StepperControl/Settings");
    cm_disconnect_experiment();
    abort();
  }



  // set up hot-links (open records) so that functions will get called
  // also get current values of these settings
  // velocity
  status = db_find_key(hDB, 0, "/Equipment/StepperControl/Settings/MotorSpeed", &gKeyMotorSpeed);
  if(status != DB_SUCCESS){
    printf("Something went wrong getting key\n"); return 0;
  }

  db_get_data(hDB, gKeyMotorSpeed, &gMotorSpeed, &size, TID_DOUBLE);

  db_open_record(hDB,gKeyMotorSpeed,&gMotorSpeed,sizeof(double),MODE_READ,
                 set_motor_speed,NULL);
  

  // speed
  status = db_find_key(hDB, 0, "/Equipment/StepperControl/Settings/MotorAccel", &gKeyMotorAccel);
  db_get_data(hDB, gKeyMotorAccel, &gMotorAccel, &size, TID_DOUBLE);
  db_open_record(hDB,gKeyMotorAccel,&gMotorAccel,sizeof(double),MODE_READ,
                 set_motor_accel,NULL);

  // destination
  status = db_find_key(hDB, 0, "/Equipment/StepperControl/Settings/Destination", &gKeyDestination);
  gDestination = 0; // set the destination to 0 to start, since that is what controller will assume.
  db_set_data(hDB, gKeyDestination, &gDestination, sizeof(int), 1, TID_INT);
  db_get_data(hDB, gKeyDestination, &gDestination, &size, TID_INT);
  db_open_record(hDB,gKeyDestination,&gDestination,sizeof(int),MODE_READ,
                 set_destination,NULL);
  
  */
  // Setup the phidget

  PhidgetReturnCode res;
  const char *errs;
  
  // Enable logging to stdout
  //  PhidgetLog_enable(PHIDGET_LOG_INFO, NULL);
  printf("Create the dig input channel\n");
  
  for(int i = 0; i < 16; i++){

    printf("Setup for input channel %i\n",i);
        
        
    res = PhidgetDigitalInput_create(&(fInputHandle[i]));
    if (res != EPHIDGET_OK) {
      fprintf(stderr, "failed to create digital input channel %i\n",i);
      exit(1);
    }
    
    Phidget_setChannel ((PhidgetHandle)fInputHandle[i], i);
    
    
    res = initChannel((PhidgetHandle)fInputHandle[i], 501372);
    if (res != EPHIDGET_OK) {
      Phidget_getErrorDescription(res, &errs);
      fprintf(stderr, "failed to initialize channel:%s\n", errs);
      exit(1);
    }
    
    
    /*
     * Open the channel synchronously: waiting a maximum of 5 seconds.
     */
    res = Phidget_openWaitForAttachment((PhidgetHandle)fInputHandle[i], 5000);
    if (res != EPHIDGET_OK) {
      if (res == EPHIDGET_TIMEOUT) {
        printf("Channel did not attach after 5 seconds: please check that the device is attached\n");
      } else {
        Phidget_getErrorDescription(res, &errs);
        fprintf(stderr, "failed to open channel:%s\n", errs);
      }
      return -1;
    }
  }
  printf("Gathering data for 10 seconds...\n");
  usleep(10000000);
  
  return SUCCESS;
}




/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  printf("Exiting feStepperControl!\n");

  for(int i = 0; i < 16; i++){
    Phidget_close((PhidgetHandle)fInputHandle[i]);
    PhidgetDigitalInput_delete(&(fInputHandle[i]));
  }
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
// Upon run stasrt, read ODB settings and write them to DCRC
INT begin_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
  usleep(1000);
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/
// Not currently used for DCRC readout
#ifdef __cplusplus
extern "C" {
#endif

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;

   for (i = 0; i < count; i++) {
//      cam_lam_read(LAM_SOURCE_CRATE(source), &lam);

//      if (lam & LAM_SOURCE_STATION(source))
         if (!test)
            return 1;
   }

   usleep(1000);
   return 0;
}
#ifdef __cplusplus
}
#endif

/*-- Interrupt configuration ---------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
  INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}
#ifdef __cplusplus
}
#endif


#include <sys/time.h>
/*-- Event readout -------------------------------------------------*/
INT read_trigger_event(char *pevent, INT off)
{

  /* init bank structure */
  bk_init32(pevent);
  
  int *pdata32;
  bk_create(pevent, "PHIO", TID_INT, (void **)&pdata32);

  printf("State.... \n");
  
  for(int i = 0; i < 16; i++){
    int state2;
    PhidgetDigitalInput_getState((fInputHandle[i]),&state2);
    printf("state %i\n",state2);
    *pdata32++ = state2;
  }
  int size2 = bk_close(pevent, pdata32);    

  

  struct timeval start,end;
  gettimeofday(&start,NULL);
  if(0)printf ("simdaq request: %f\n",start.tv_sec
      + 0.000001*start.tv_usec); 
  // close the bank

  
  usleep(1000);
  return bk_size(pevent);
}



